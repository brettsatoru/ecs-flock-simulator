﻿using UnityEngine;
using Entitas;
using Entitas.CodeGeneration.Attributes;

[Game, Event(EventTarget.Self)]
public class ForwardComponent : IComponent
{
    public Vector3 value;
}
