using Entitas;

public sealed class RootSystems : Feature 
{
    public RootSystems(Contexts contexts)
    {
        // Init
        Add(new LoadAssetSystem(contexts));

        // Input

        // Update
        Add(new LogHealthSystem(contexts));
        Add(new HealthSystem(contexts));
        Add(new ApplyVelocitySystem(contexts));
        
        // View / Render
        Add(new PositionEventSystem(contexts));
        Add(new ForwardEventSystem(contexts));
        
        // Cleanup
        Add(new DestroyEntitySystem(contexts));
    }
}