﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Entitas;

public class UnityViewService : IViewService
{
    public void LoadAsset(Contexts contexts, IEntity entity, string assetName)
    {
        var viewGo = GameObject.Instantiate(Resources.Load<GameObject>("Prefabs/" + assetName));
        if (viewGo != null)
        {
            var viewController = viewGo.GetComponent<IViewController>();
            if (viewController != null)
            {
                viewController.InitializeView(contexts, entity);
            }
        }

        var eventListeners = viewGo.GetComponents<IEventListener>();
        foreach (var listener in eventListeners)
        {
            listener.RegisterListeners(entity);
        }
    }
}
