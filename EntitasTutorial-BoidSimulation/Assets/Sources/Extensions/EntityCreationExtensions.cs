using UnityEngine;

public static class EntityCreationExtensions
{
    public static GameEntity CreateFlockEntity(this Contexts contexts, string prefab, Vector3 position, Vector3 velocity)
    {
        var e = contexts.game.CreateEntity();
        e.AddAsset(prefab, false);
        e.AddPosition(position);
        e.AddForward(velocity.normalized);
        e.AddVelocity(velocity);
        // TODO: Add Agent Behavior
        e.isFlockAgent = true;
        return e;
    }
}