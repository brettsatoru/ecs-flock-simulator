using System;

public interface ICollisionEmitter
{
    Action<UnityEngine.Collision> OnCollisionEnter { get; set;}
    Action<UnityEngine.Collision> OnCollisionExit {get; set;}
    Action<UnityEngine.Collision> OnCollisionStay {get; set;}
}