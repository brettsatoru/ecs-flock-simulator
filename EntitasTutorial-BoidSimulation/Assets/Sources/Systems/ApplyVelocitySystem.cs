using System.Collections.Generic;
using Entitas;

public sealed class ApplyVelocitySystem : IExecuteSystem
{
    ICollector<GameEntity> _velocityEntities;

    public ApplyVelocitySystem(Contexts contexts)
    {
        _velocityEntities = contexts.game.CreateCollector(GameMatcher.Velocity);
        _velocityEntities.Activate();
    }

    public void Execute()
    {
        foreach (var e in _velocityEntities.collectedEntities)
        {
            if (e.hasForward)
            {
                e.ReplaceForward(e.velocity.value.normalized);
            }

            if (e.hasPosition)
            {
                // Need to get Time System together, instead of using UnityEngine
                e.ReplacePosition(e.position.value + (e.velocity.value * UnityEngine.Time.deltaTime));
            }
        }
    }
}