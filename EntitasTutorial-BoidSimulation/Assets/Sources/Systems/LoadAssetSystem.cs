﻿using System.Collections.Generic;
using Entitas;

public class LoadAssetSystem : ReactiveSystem<GameEntity>, IInitializeSystem
{
    readonly Contexts _contexts;
    IViewService _viewService;

    public LoadAssetSystem(Contexts context) : base(context.game)
    {
        _contexts = context;
    }

    public void Initialize()
    {
        _viewService = _contexts.meta.viewService.instance;
    }

    protected override void Execute(List<GameEntity> entities)
    {
        foreach (var e in entities)
        {
            _viewService.LoadAsset(_contexts, e, e.asset.name);
            e.asset.isLoaded = true;
        }
    }

    protected override bool Filter(GameEntity entity)
    {
        return entity.hasAsset && !entity.asset.isLoaded;
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
    {
        return context.CreateCollector(GameMatcher.Asset);
    }
}
