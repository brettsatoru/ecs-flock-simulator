using Entitas;

public sealed class DestroyEntitySystem : IExecuteSystem
{
    readonly Contexts _contexts;

    public DestroyEntitySystem(Contexts contexts)
    {
        _contexts = contexts;
    }

    public void Execute()
    {
        foreach (var e in _contexts.game.GetEntities(GameMatcher.Destroyed))
        {
            e.Destroy();
        }
    }
}