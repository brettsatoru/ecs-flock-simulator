using UnityEngine;
using Entitas;

public sealed class CreatePlayerSystem : IInitializeSystem
{
    readonly Contexts _contexts;

    public CreatePlayerSystem(Contexts contexts)
    {
        _contexts = contexts;
    }

    public void Initialize()
    {
        var e = _contexts.game.CreateEntity();
        e.AddAsset("player", false);
        e.AddHealth(100);
        e.AddPosition(Vector3.zero);
        e.AddForward(Vector3.forward);
        e.AddVelocity(Vector3.forward);
    }
}