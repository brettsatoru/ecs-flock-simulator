﻿using UnityEngine;
using Entitas;

public class ForwardListener : MonoBehaviour, IForwardListener, IEventListener
{
    GameEntity _entity;

    public void OnForward(GameEntity entity, Vector3 newValue)
    {
        Debug.Log($"Setting forward to {newValue}");
        transform.forward = newValue;
    }

    public void RegisterListeners(IEntity entity)
    {
        Debug.Log("Registering to forward");
        _entity = entity as GameEntity;
        _entity.AddForwardListener(this);
    }
}
