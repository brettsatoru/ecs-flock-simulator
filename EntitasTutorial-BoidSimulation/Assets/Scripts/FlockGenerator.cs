using Sirenix.OdinInspector;
using UnityEngine;
using Entitas.Unity;

public class FlockGenerator : MonoBehaviour
{

    const float agentDensity = 0.08f;

    [SerializeField] [Range(10, 500)] private int StartingFlock = 250;
    [SerializeField] [Range(1f, 100f)] private float DriveFactor = 10f;
    [SerializeField] [Range(1f, 100f)] private float MaxSpeed = 5f;
    [SerializeField] [Range(1f, 10f)] private float NeighborRadius = 1.5f;
    [SerializeField] [Range(0f, 1f)] private float AvoidanceRadiusMultiplier = 0.5f;

    int StartingFlockGenerated = 0;
    float squareMaxSpeed;
    float squareNeighborRadius;
    float squareAvoidanceRadius;
    public float SquareAvoidanceRadius {get => squareAvoidanceRadius;}

    void Start()
    {
        squareMaxSpeed = Mathf.Pow(MaxSpeed, 2);
        squareNeighborRadius = Mathf.Pow(NeighborRadius, 2);
        squareAvoidanceRadius = Mathf.Pow(NeighborRadius * AvoidanceRadiusMultiplier, 2);

        // Generate a FlockConfig Entity?
    }

    void Update()
    {
        if (StartingFlockGenerated < StartingFlock)
        {
            Contexts contexts = Contexts.sharedInstance;
            var e = contexts.CreateFlockEntity("agent", Random.insideUnitSphere * StartingFlock * agentDensity, Vector3.forward * Random.Range(0f, 360f));
            StartingFlockGenerated++;
        }
    }

    [Button]
    public void GenerateFlockAgent()
    {
        if (Application.isEditor && !Application.isPlaying) return;
        Contexts.sharedInstance.CreateFlockEntity("agent", Random.insideUnitSphere * StartingFlock * agentDensity, Vector3.forward * Random.Range(0f, 360f));
    }
}