﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{

    private ServiceRegistrationSystems _services;
    private RootSystems _system;

    // Start is called before the first frame update
    void Start()
    {
        var contexts = Contexts.sharedInstance;

        var allServices= new Services(
            new UnityViewService()
        );

        _services = new ServiceRegistrationSystems(contexts, allServices);
        _services.Initialize();

        _system = new RootSystems(contexts);
        _system.Initialize();
    }

    void Update()
    {
        _services.Execute();
        _system.Execute();
    }

    void LateUpdate()
    {
        _services.Cleanup();
        _system.Cleanup();
    }
}
