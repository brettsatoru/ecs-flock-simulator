﻿using System.Collections;
using System.Collections.Generic;
using Entitas;
using UnityEngine;

public class PositionListener : MonoBehaviour, IEventListener, IPositionListener
{
    GameEntity _entity;

    public void OnPosition(GameEntity entity, Vector3 newPosition)
    {
        transform.position = newPosition;
    }

    public void RegisterListeners(IEntity entity)
    {
        _entity = entity as GameEntity;
        _entity.AddPositionListener(this);
    }
}
