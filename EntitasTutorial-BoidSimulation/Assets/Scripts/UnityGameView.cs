﻿using System.Collections;
using System.Collections.Generic;
using Entitas;
using Entitas.Unity;
using UnityEngine;

public class UnityGameView : MonoBehaviour, IViewController
{
    protected Contexts _contexts;
    protected GameEntity _entity;

    public Vector2 Position { get => transform.position; set => transform.position = value; }
    public Vector2 Scale { get => transform.localScale; set => transform.localScale = value; }
    public bool Active { get => gameObject.activeSelf; set => gameObject.SetActive(value); }

    public void DestroyView()
    {
        Object.Destroy(this);
    }

    public void InitializeView(Contexts contexts, IEntity entity)
    {
        _contexts = contexts;
        _entity = entity as GameEntity;
        gameObject.Link(entity);
    }

    void Destroy()
    {
        gameObject.Unlink();
    }
}
